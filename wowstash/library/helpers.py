import requests

from wowstash.models import Event
from wowstash.factory import db
from wowstash import config


def capture_event(user_id, event_type):
    event = Event(
        user=user_id,
        type=event_type
    )
    db.session.add(event)
    db.session.commit()
    return

def post_discord_webhook(text):
    try:
        r = requests.post(config.DISCORD_URL, data={"content": text})
        r.raise_for_status()
        return True
    except:
        return False
