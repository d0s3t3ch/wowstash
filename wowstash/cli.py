import click
from flask import Blueprint, url_for

import wowstash.models
from wowstash.library.docker import docker
from wowstash.models import User, PasswordReset, Event
from wowstash.factory import db, bcrypt


bp = Blueprint("cli", "cli", cli_group=None)


@bp.cli.command('clean_containers')
def clean_containers():
    docker.cleanup()

@bp.cli.command('reset_wallet')
@click.argument('user_id')
def reset_wallet(user_id):
    user = User.query.get(user_id)
    user.clear_wallet_data()
    print(f'Wallet data cleared for user {user.id}')

@bp.cli.command('init')
def init():
    db.create_all()

@bp.cli.command('list_users')
def list_users():
    users = User.query.all()
    for i in users:
        print(f'{i.id} - {i.email}')

@bp.cli.command('wipe_user')
@click.argument('user_id')
def wipe_user(user_id):
    user = User.query.get(user_id)
    if user:
        events = Event.query.filter(Event.user == user.id)
        for i in events:
            print(f'[+] Deleting event {i.id} for user {user.id}')
            db.session.delete(i)
        print(f'[+] Deleting user {user.id}')
        db.session.delete(user)
        db.session.commit()
        return True
    else:
        print('That user id does not exist')
        return False

@bp.cli.command('reset_password')
@click.argument('user_email')
@click.argument('duration')
def reset_password(user_email, duration):
    user = User.query.filter(User.email==user_email).first()
    if not user:
        click.echo('[!] Email address does not exist!')
        return

    pwr = PasswordReset(
        user=user.id,
        hash=PasswordReset().generate_hash(),
        expiration_hours=duration
    )
    db.session.add(pwr)
    db.session.commit()
    click.echo(f'[+] Password reset link #{pwr.id} for {user_email} expires in {duration} hours: {url_for("auth.reset", hash=pwr.hash)}')
